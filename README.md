
![To-do App Logo](public/images/todo_logo.svg)

#### [Live Demo](http://todo.bkevans.com)

An application a user can use to list, sort, and filter various things they need to do...built in Vanilla JavaScript. Styling is written in Sass and compiled with Webpack. Webpack also handles compiling the ES6 JavaScript into browser-friendly ES5 with the help of Babel. User to-do data is stored in localStorage in the user's browser.      

## Getting Started

### Prerequisites

Make sure that you have [Node.js](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed.

To check the versions you have installed, run:  

```
node -v && npm -v
```

### Installing

First you'll need to clone the source files onto your local machine. 

To clone you can run:

```
git clone https://bkevans@bitbucket.org/bkevans/todo-app.git
```

Once cloned, move into the app directory:

```
cd todo-app/
```

Now you'll need to install of the necessary dependencies to run the app locally using Yarn: 

```
npm install
```

With all of the dependencies installed, you're ready to run a development build and spin up a development server for the app by running: 

```
npm run dev-server
```

This will build the app, and run spin up a local Webpack server. The output from this command will provide a URL where the app can be accessed in your browser, e.g. *http://localhost:8080/*

Now that the app is up and running locally, you can crack open the /src directory and play around with the code or styling. Changes will be rendered in the browser immediately as long as the development server is running. 

To shut down the dev server, simply press [cntrl + c] 

## Deployment

This project uses a Bitbucket deployment pipeline to deploy to an S3 bucket. 

In order to use this method, you'll need to set up an S3 bucket, generate a role with keys and write privelages for your S3 bucket, and set up the keys in your Bitbucket repo. A good guide for setting this up can be found [here](https://medium.com/@mabdullahabid/how-to-use-bitbucket-pipelines-for-continuous-deployment-to-amazon-s3-aeb4b3e1f282)

Once you have configured your S3 bucket and added your access keys to your bitbucket repo, simply edit the *bitbucket-pipelines.yml* file in this project with the region and endpoint for your S3 bucket.

The file is currently set up to deploy the /dist directory whenever a git commit is made to the master branch and pushed to the repository.

To create a production build of this app stop your dev server (if it is still running [cntrl + c], and run:  

```
 npm run build
```

A production build will clean and minify JavaScript and CSS in the /public directory of the app. 

Simply commit your changes and push to your repo's master branch to initiate a deployment to your S3 bucket. Once complete you will have an updated app version that you can share with the world.  

## Built With

* [React](https://reactjs.org/) - The UI library used
* [Babel](https://babeljs.io/) - As a JavaScript compiler
* [Sass](https://sass-lang.com/) - To make things look interesting
* [Webpack](https://webpack.js.org/) - Used as a module bundler

## License

This project is licensed under the MIT License

## Acknowledgments

This project was initially built while participating in an excellent [Udemy course](https://www.udemy.com/modern-javascript/) provided by Andrew Mead. A huge thank you to Andrew for sharing his knowledge and helping others grow their developement toolkit!

