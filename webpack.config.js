const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'scripts/bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    }, {
				test: /\.scss$/,
				use: [
					'style-loader',
					MiniCssExtractPlugin.loader,
					'css-loader',
          'postcss-loader',
					'sass-loader'
				]
			}
    ]
  },
  plugins: [
		new MiniCssExtractPlugin({
			filename: 'styles/styles.css'
		})
	],
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    publicPath: '/'
  },
  devtool: 'source-map'
}
