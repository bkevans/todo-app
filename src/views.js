import { getTodos, toggleTodo, removeTodo } from './todos.js';
import { getFilters } from './filters.js';

// renderTodos
const renderTodos = () => {
  const todoEl = document.querySelector('#todos');
  const { searchText, hideCompleted } = getFilters();
  const filteredTodos = getTodos().filter(todo => {
		const searchTextMatch = todo.text
			.toLowerCase()
			.includes(searchText.toLowerCase());
		const hideCompletedMatch = !hideCompleted || !todo.completed;
		return searchTextMatch && hideCompletedMatch;
	});

  const incompleteTodos = filteredTodos.filter(todo => !todo.completed);
  todoEl.innerHTML = '';
  todoEl.appendChild(generateSummaryDOM(incompleteTodos));

  if (filteredTodos.length > 0) {
    filteredTodos.forEach(todo => {
      todoEl.appendChild(generateTodoDOM(todo));
    });
  } else {
    const messageEl = document.createElement('p');
    messageEl.classList.add('empty-message');
    messageEl.textContent = 'Nothing to see here...';
    todoEl.appendChild(messageEl);
  }
};


// Get DOM elements for todos
const generateTodoDOM = todo => {
  const todoEl = document.createElement('label');
  const containerEl = document.createElement('div');
  const checkbox = document.createElement('input');
  const todoText = document.createElement('span');
  const removeButton = document.createElement('button');

  // Setup todo checkbox
  checkbox.setAttribute('type', 'checkbox');
  checkbox.checked = todo.completed;
  containerEl.appendChild(checkbox);
  checkbox.addEventListener('change', e => {
    toggleTodo(todo.id);
    renderTodos();
  });

  // Setup todo text
  todoText.textContent = todo.text;
  todo.completed ? todoEl.classList.add('completed') : todoEl.classList.remove('completed');

  containerEl.appendChild(todoText);

  // Setup container
  todoEl.classList.add('list-item');
  containerEl.classList.add('list-item__container');
  todoEl.appendChild(containerEl);

  // Setup remove button
  removeButton.textContent = 'remove';
  removeButton.classList.add('button', 'button--text');
  todoEl.appendChild(removeButton);
  removeButton.addEventListener('click', e => {
    removeTodo(todo.id);
    renderTodos();
  });

  return todoEl;
};

// Get DOM elements for the summary
const generateSummaryDOM = incompleteTodos => {
  const summary = document.createElement('h2');
  summary.classList.add('list-title');
  const plural = incompleteTodos.length === 1 ? '' : 's';

  if (incompleteTodos.length > 0) {
    summary.textContent = `You\'ve got ${incompleteTodos.length} thing${plural} left to do.`;
  } else {
    summary.textContent = "You're all caught up.";
  }
  return summary;
};

export { renderTodos, generateSummaryDOM, generateTodoDOM };