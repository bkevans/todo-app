import uuidv4 from 'uuid/v4';

// Setup the empty todos array
let todos = [];

// Fetch exisiting todos
const loadTodos = () => {
	const todosJSON = localStorage.getItem('todos');
	try {
		todos = todosJSON ? JSON.parse(todosJSON) : [];
	} catch (e) {
		todos = [];
	}
};

// Save todos
const saveTodos = () => {
	localStorage.setItem('todos', JSON.stringify(todos));
};

// Get todos
const getTodos = () => todos;

// Create a todo
const createTodo = text => {
	todos.push({
    id: uuidv4(),
		text,
		completed: false
	});
	saveTodos();
};

// Remove a todo
const removeTodo = id => {
	const todoIndex = todos.findIndex(todo => todo.id === id);

	if (todoIndex > -1) {
		todos.splice(todoIndex, 1);
    saveTodos();
	}
};

// Toggle todo completion
const toggleTodo = id => {
	const todo = todos.find(todo => todo.id === id);

	if (todo) {
		todo.completed = !todo.completed;
    saveTodos();
	}
};

loadTodos();

export { loadTodos, getTodos, createTodo, removeTodo, toggleTodo };
