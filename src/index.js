import './scss/main.scss';
import { renderTodos } from './views.js';
import { setFilters } from './filters';
import { loadTodos, createTodo } from './todos';

renderTodos();

// Listen for seach text change
document.querySelector('#search-text').addEventListener('input', e => {
  setFilters({ searchText: e.target.value });
  renderTodos();
});

// Listen for new todo submission
document.querySelector('#new-todo').addEventListener('submit', e => {
  e.preventDefault();
  let text = e.target.elements.todoInput.value.trim();

  if (text.length > 0) {
    createTodo(text);
    renderTodos();
    e.target.elements.todoInput.value = '';
  }
});

// Listen for hide completed todos
document.querySelector('#hide-completed').addEventListener('change', e => {
  setFilters({ hideCompleted: e.target.checked });
	renderTodos();
});

window.addEventListener('storage', e => {
	if (e.key === 'todos') {
    loadTodos();
		renderTodos();
	}
});